% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/mapCanvas.R
\name{mapCanvas.center}
\alias{mapCanvas.center}
\title{Get map center, in geographical coordinates.}
\usage{
mapCanvas.center(...)
}
\arguments{
\item{...}{low-level parameters (such as \code{host} and \code{auth}) to be passed on to \code{\link[=qgisremote]{qgisremote()}}}
}
\value{
A numeric vector of length two.
}
\description{
Get map center, in geographical coordinates.
}
