# qgisremote

[![CRAN\_Status\_Badge](http://www.r-pkg.org/badges/version/qgisremote)](https://qgisapi.gitlab.io/qgisremote/)

This R package provides access to [QGIS Desktop](http://qgis.org)'s data processing and canvas drawing functionalities through the Web API provided by the Network API plugin, all currently under development as part of the [Google Summer of Code 2017](https://summerofcode.withgoogle.com/projects/#5197021490184192).

HTML documentation for all currently implemented functions is available online [here](http://qgisapi.gitlab.io/qgisremote/reference/).

## Installation

In order to install the latest development version you first need the [devtools](https://CRAN.R-project.org/package=devtools) package

    install.packages("devtools")

then install the latest package version by calling

    devtools::install_git('https://gitlab.com/qgisapi/qgisremote.git', quiet = FALSE)

#### External dependencies

The `sf` package that is used for geodata processing relies on a number of external libraries, in particular `GDAL`, `GEOS`, `Proj.4`, as well as the `udunits2` library.

If the installation of the `udunits2` R package still fails, you might have to [explicitly specify the path to the library](https://gist.github.com/giocomai/0a94e49b3dad053292e73fce7feda411).

<!--
`geojsonio` might be preferrable over `rgdal` for geojson conversion but it depends on V8 which requires additional libraries (`v8-314-devel` on Debian/Fedora) to be pre-installed. Its dependency `protolite` also requires the Protocol Buffers library distributed under the `protobuf` (or `protobuf-dev`) package names. -->

#### QGIS plugin

This package is of no use without a QGIS Network API instance to communicate with. Unless you're accessing an instance of QGIS running on somebody else's machine, you'll probably want to install the *QGIS Network API/Remote Control plugin*. Installation instructions can be found [here](https://gitlab.com/qgisapi/networkapi#testing).

### Building this documentation website

The function reference and articles (R package vignettes) can be built using the `pkgdown` package which can be installed from github:


    devtools::install_github("hadley/pkgdown")

    # call from the qgisremote package directory
    pkgdown::build_site()


Please note that building of the articles depends on having an instance of the QGIS Network API plugin running locally on its default port, and that generating them might take a while. It is also possible to generate the function reference and articles seperately, by calling `pkgdown::build_reference()` and `pkgdown::build_articles()` respectively. The index page you are looking at is generated from the `README.md` file by calling `pkgdown::build_home()`.

The list of functions in the reference is generated according to the specification in the `_pkgdown.yml` file that can be found in the root directory. Many of the examples for the different functions are not executed by default (marked as `\dontrun{}` in the source). If you have an instance of the network API plugin running locally on the default port, you can include the results for these examples in the generated reference by calling

    pkgdown::build_reference(run_dont_run = TRUE)
